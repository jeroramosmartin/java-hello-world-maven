<?xml version="1.0" encoding="UTF-8"?><sld:StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:sld="http://www.opengis.net/sld" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc" version="1.0.0">
    <sld:NamedLayer>
        <sld:Name>AU.AdministrativeUnit</sld:Name>
        <sld:UserStyle>
            <sld:Name>UnidadesAdministrativas</sld:Name>
            <sld:Title>IGN: Estilo de unidadeas administrativas</sld:Title>
            <sld:IsDefault>1</sld:IsDefault>
            <sld:Abstract>Estilo de representación del IGN de unidades administrativas.</sld:Abstract>
            <sld:FeatureTypeStyle>
                <sld:Name>UnidadesAdministrativas</sld:Name>
                <sld:Title>IGN: Estilo de Comunidades Autonómas</sld:Title>
                <sld:Abstract>Estilo de representación del IGN de unidades administrativas.</sld:Abstract>
                <sld:Rule>
                    <sld:Name>ComunidadAutonoma</sld:Name>
                    <sld:Abstract>Estilo de representación de límite administrativo de Comunidades Autonómas.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                            <ogc:PropertyName>nationallevel</ogc:PropertyName>
                            <ogc:Literal>%2ndOrder</ogc:Literal>
                        </ogc:PropertyIsLike>
                    </ogc:Filter>
                    <sld:MinScaleDenominator>5000000.0</sld:MinScaleDenominator>
                    <sld:PolygonSymbolizer>
                        <sld:Stroke>
                            <sld:CssParameter name="stroke">#FFFF8E</sld:CssParameter>
                            <sld:CssParameter name="stroke-linecap">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-linejoin">round</sld:CssParameter>
                        </sld:Stroke>
                    </sld:PolygonSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>ComunidadAutonoma</sld:Name>
                    <sld:Abstract>Estilo de representación de límite administrativo de Comunidades Autonómas.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                            <ogc:PropertyName>nationallevel</ogc:PropertyName>
                            <ogc:Literal>%2ndOrder</ogc:Literal>
                        </ogc:PropertyIsLike>
                    </ogc:Filter>
                    <sld:MaxScaleDenominator>5000000.0</sld:MaxScaleDenominator>
                    <sld:PolygonSymbolizer>
                        <sld:Stroke>
                            <sld:CssParameter name="stroke">#FFFF8E</sld:CssParameter>
                            <sld:CssParameter name="stroke-linecap">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-linejoin">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-width">2.0</sld:CssParameter>
                        </sld:Stroke>
                    </sld:PolygonSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>ETI_COMUNIDADES_AUTONOMAS</sld:Name>
                    <sld:Abstract>Rotulación  de etiquetas de CCAA entre las escalas 1:15M y 1:10M. Tamaño de letra 6  y no se etiquetan las conjurisdicciones autonómicas</sld:Abstract>
                    <ogc:Filter>
                        <ogc:And>
                            <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                                <ogc:PropertyName>nationallevel</ogc:PropertyName>
                                <ogc:Literal>%2ndOrder</ogc:Literal>
                            </ogc:PropertyIsLike>
                            <ogc:Not>
                                <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                                    <ogc:PropertyName>nameunit</ogc:PropertyName>
                                    <ogc:Literal>Conjurisdicciones%</ogc:Literal>
                                </ogc:PropertyIsLike>
                            </ogc:Not>
                        </ogc:And>
                    </ogc:Filter>
                    <sld:MinScaleDenominator>1.0E7</sld:MinScaleDenominator>
                    <sld:MaxScaleDenominator>1.5E7</sld:MaxScaleDenominator>
                    <sld:TextSymbolizer>
                        <sld:Label>
                            <ogc:PropertyName>nameunit</ogc:PropertyName>
                        </sld:Label>
                        <sld:Font>
                            <sld:CssParameter name="font-family">Arial</sld:CssParameter>
                            <sld:CssParameter name="font-size">50.0</sld:CssParameter>
                            <sld:CssParameter name="font-style">normal</sld:CssParameter>
                            <sld:CssParameter name="font-weight">normal</sld:CssParameter>
                        </sld:Font>
                        <sld:LabelPlacement>
                            <sld:PointPlacement>
                                <sld:AnchorPoint>
                                    <sld:AnchorPointX>0.5</sld:AnchorPointX>
                                    <sld:AnchorPointY>0.5</sld:AnchorPointY>
                                </sld:AnchorPoint>
                                <sld:Displacement>
                                    <sld:DisplacementX>0.0</sld:DisplacementX>
                                    <sld:DisplacementY>0.0</sld:DisplacementY>
                                </sld:Displacement>
                            </sld:PointPlacement>
                        </sld:LabelPlacement>
                        <sld:Halo>
                            <sld:Radius>0.5</sld:Radius>
                            <sld:Fill>
                                <sld:CssParameter name="fill">#FFFFFF</sld:CssParameter>
                            </sld:Fill>
                        </sld:Halo>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#000000</sld:CssParameter>
                        </sld:Fill>
                        <sld:VendorOption name="group">true</sld:VendorOption>
                        <sld:VendorOption name="autoWrap">70</sld:VendorOption>
                        <sld:VendorOption name="goodnessOfFit">0.0</sld:VendorOption>
                    </sld:TextSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>ETI_COMUNIDADES_AUTONOMAS</sld:Name>
                    <sld:Abstract>Rotulación  de etiquetas de CCAA entre las escalas 1:10M y 1:5M. Tamaño de letra 8 y  no se etiquetan las conjurisdicciones autonómicas</sld:Abstract>
                    <ogc:Filter>
                        <ogc:And>
                            <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                                <ogc:PropertyName>nationallevel</ogc:PropertyName>
                                <ogc:Literal>%2ndOrder</ogc:Literal>
                            </ogc:PropertyIsLike>
                            <ogc:Not>
                                <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                                    <ogc:PropertyName>nameunit</ogc:PropertyName>
                                    <ogc:Literal>Conjurisdicciones%</ogc:Literal>
                                </ogc:PropertyIsLike>
                            </ogc:Not>
                        </ogc:And>
                    </ogc:Filter>
                    <sld:MinScaleDenominator>5000000.0</sld:MinScaleDenominator>
                    <sld:MaxScaleDenominator>1.0E7</sld:MaxScaleDenominator>
                    <sld:TextSymbolizer>
                        <sld:Label>
                            <ogc:PropertyName>nameunit</ogc:PropertyName>
                        </sld:Label>
                        <sld:Font>
                            <sld:CssParameter name="font-family">Arial</sld:CssParameter>
                            <sld:CssParameter name="font-size">50.0</sld:CssParameter>
                            <sld:CssParameter name="font-style">normal</sld:CssParameter>
                            <sld:CssParameter name="font-weight">normal</sld:CssParameter>
                        </sld:Font>
                        <sld:LabelPlacement>
                            <sld:PointPlacement>
                                <sld:AnchorPoint>
                                    <sld:AnchorPointX>0.5</sld:AnchorPointX>
                                    <sld:AnchorPointY>0.5</sld:AnchorPointY>
                                </sld:AnchorPoint>
                                <sld:Displacement>
                                    <sld:DisplacementX>0.0</sld:DisplacementX>
                                    <sld:DisplacementY>0.0</sld:DisplacementY>
                                </sld:Displacement>
                            </sld:PointPlacement>
                        </sld:LabelPlacement>
                        <sld:Halo>
                            <sld:Radius>1.0</sld:Radius>
                            <sld:Fill>
                                <sld:CssParameter name="fill">#FFFFFF</sld:CssParameter>
                                <sld:CssParameter name="fill-opacity">0.9</sld:CssParameter>
                            </sld:Fill>
                        </sld:Halo>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#000000</sld:CssParameter>
                        </sld:Fill>
                        <sld:VendorOption name="group">true</sld:VendorOption>
                        <sld:VendorOption name="autoWrap">70</sld:VendorOption>
                        <sld:VendorOption name="goodnessOfFit">0.0</sld:VendorOption>
                        <sld:VendorOption name="maxDisplacement">300</sld:VendorOption>
                    </sld:TextSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>Provincias</sld:Name>
                    <sld:Abstract>Estilo de representación de los límites administrativos provinciales.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                            <ogc:PropertyName>nationallevel</ogc:PropertyName>
                            <ogc:Literal>%3rdOrder</ogc:Literal>
                        </ogc:PropertyIsLike>
                    </ogc:Filter>
                    <sld:MaxScaleDenominator>5000000.0</sld:MaxScaleDenominator>
                    <sld:PolygonSymbolizer>
                        <sld:Stroke>
                            <sld:CssParameter name="stroke">#FFFF8E</sld:CssParameter>
                            <sld:CssParameter name="stroke-linecap">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-linejoin">round</sld:CssParameter>
                        </sld:Stroke>
                    </sld:PolygonSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>ETI_PROVINCIAS</sld:Name>
                    <sld:Abstract>Escala  entre 1:5M y 1:2M. Estilo de representación de las etiquetas relativas a  los límites administrativos provinciales.No se etiquetan las  conjurisdicciones</sld:Abstract>
                    <ogc:Filter>
                        <ogc:And>
                            <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                                <ogc:PropertyName>nationallevel</ogc:PropertyName>
                                <ogc:Literal>%3rdOrder</ogc:Literal>
                            </ogc:PropertyIsLike>
                            <ogc:Not>
                                <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                                    <ogc:PropertyName>nameunit</ogc:PropertyName>
                                    <ogc:Literal>Conjurisdicciones%</ogc:Literal>
                                </ogc:PropertyIsLike>
                            </ogc:Not>
                        </ogc:And>
                    </ogc:Filter>
                    <sld:MinScaleDenominator>2000000.0</sld:MinScaleDenominator>
                    <sld:MaxScaleDenominator>5000000.0</sld:MaxScaleDenominator>
                    <sld:TextSymbolizer>
                        <sld:Label>
                            <ogc:PropertyName>nameunit</ogc:PropertyName>
                        </sld:Label>
                        <sld:Font>
                            <sld:CssParameter name="font-family">Arial</sld:CssParameter>
                            <sld:CssParameter name="font-size">50.0</sld:CssParameter>
                            <sld:CssParameter name="font-style">normal</sld:CssParameter>
                            <sld:CssParameter name="font-weight">normal</sld:CssParameter>
                        </sld:Font>
                        <sld:LabelPlacement>
                            <sld:PointPlacement>
                                <sld:AnchorPoint>
                                    <sld:AnchorPointX>0.5</sld:AnchorPointX>
                                    <sld:AnchorPointY>0.5</sld:AnchorPointY>
                                </sld:AnchorPoint>
                                <sld:Displacement>
                                    <sld:DisplacementX>0.0</sld:DisplacementX>
                                    <sld:DisplacementY>0.0</sld:DisplacementY>
                                </sld:Displacement>
                            </sld:PointPlacement>
                        </sld:LabelPlacement>
                        <sld:Halo>
                            <sld:Radius>2.0</sld:Radius>
                            <sld:Fill>
                                <sld:CssParameter name="fill">#FFFFFF</sld:CssParameter>
                                <sld:CssParameter name="fill-opacity">0.9</sld:CssParameter>
                            </sld:Fill>
                        </sld:Halo>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#000000</sld:CssParameter>
                        </sld:Fill>
                        <sld:Priority>
                            <ogc:PropertyName>nameunit</ogc:PropertyName>
                        </sld:Priority>
                        <sld:VendorOption name="goodnessOfFit">0.0</sld:VendorOption>
                        <sld:VendorOption name="group">true</sld:VendorOption>
                        <sld:VendorOption name="autoWrap">70</sld:VendorOption>
                    </sld:TextSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>ETI_PROVINCIAS</sld:Name>
                    <sld:Abstract>Escala  entre 1:2M y 1:0.8M. Estilo de representación de las etiquetas  relativas a los límites administrativos provinciales.Sí se etiquetan las  conjurisdicciones</sld:Abstract>
                    <ogc:Filter>
                        <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                            <ogc:PropertyName>nationallevel</ogc:PropertyName>
                            <ogc:Literal>%3rdOrder</ogc:Literal>
                        </ogc:PropertyIsLike>
                    </ogc:Filter>
                    <sld:MinScaleDenominator>800000.0</sld:MinScaleDenominator>
                    <sld:MaxScaleDenominator>2000000.0</sld:MaxScaleDenominator>
                    <sld:TextSymbolizer>
                        <sld:Label>
                            <ogc:PropertyName>nameunit</ogc:PropertyName>
                        </sld:Label>
                        <sld:Font>
                            <sld:CssParameter name="font-family">Arial</sld:CssParameter>
                            <sld:CssParameter name="font-size">11.0</sld:CssParameter>
                            <sld:CssParameter name="font-style">normal</sld:CssParameter>
                            <sld:CssParameter name="font-weight">normal</sld:CssParameter>
                        </sld:Font>
                        <sld:LabelPlacement>
                            <sld:PointPlacement>
                                <sld:AnchorPoint>
                                    <sld:AnchorPointX>0.5</sld:AnchorPointX>
                                    <sld:AnchorPointY>0.5</sld:AnchorPointY>
                                </sld:AnchorPoint>
                                <sld:Displacement>
                                    <sld:DisplacementX>0.0</sld:DisplacementX>
                                    <sld:DisplacementY>0.0</sld:DisplacementY>
                                </sld:Displacement>
                            </sld:PointPlacement>
                        </sld:LabelPlacement>
                        <sld:Halo>
                            <sld:Radius>1.0</sld:Radius>
                            <sld:Fill>
                                <sld:CssParameter name="fill">#FFFFFF</sld:CssParameter>
                                <sld:CssParameter name="fill-opacity">0.9</sld:CssParameter>
                            </sld:Fill>
                        </sld:Halo>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#000000</sld:CssParameter>
                        </sld:Fill>
                        <sld:Priority>
                            <ogc:PropertyName>nameunit</ogc:PropertyName>
                        </sld:Priority>
                        <sld:VendorOption name="goodnessOfFit">0.0</sld:VendorOption>
                        <sld:VendorOption name="group">true</sld:VendorOption>
                        <sld:VendorOption name="autoWrap">70</sld:VendorOption>
                    </sld:TextSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>Municipios</sld:Name>
                    <sld:Title>Municipios</sld:Title>
                    <sld:Abstract>Estilo de representaci?n de los l?mites administrativos municipales.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                            <ogc:PropertyName>nationallevel</ogc:PropertyName>
                            <ogc:Literal>%4thOrder</ogc:Literal>
                        </ogc:PropertyIsLike>
                    </ogc:Filter>
                    <sld:MaxScaleDenominator>600000.0</sld:MaxScaleDenominator>
                    <sld:PolygonSymbolizer>
                        <sld:Stroke>
                            <sld:CssParameter name="stroke">#FFFF8E</sld:CssParameter>
                            <sld:CssParameter name="stroke-linecap">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-linejoin">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-width">0.3</sld:CssParameter>
                        </sld:Stroke>
                    </sld:PolygonSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>ETI_MUNICIPIOS</sld:Name>
                    <sld:Abstract>Estilo de representación de las etiquetas relativas a los límites administrativos municipales.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                            <ogc:PropertyName>nationallevel</ogc:PropertyName>
                            <ogc:Literal>%4thOrder</ogc:Literal>
                        </ogc:PropertyIsLike>
                    </ogc:Filter>
                    <sld:MaxScaleDenominator>800000.0</sld:MaxScaleDenominator>
                    <sld:TextSymbolizer>
                        <sld:Label>
                            <ogc:PropertyName>nameunit</ogc:PropertyName>
                        </sld:Label>
                        <sld:Font>
                            <sld:CssParameter name="font-family">Arial</sld:CssParameter>
                            <sld:CssParameter name="font-size">50.0</sld:CssParameter>
                            <sld:CssParameter name="font-style">normal</sld:CssParameter>
                            <sld:CssParameter name="font-weight">normal</sld:CssParameter>
                        </sld:Font>
                        <sld:LabelPlacement>
                            <sld:PointPlacement>
                                <sld:AnchorPoint>
                                    <sld:AnchorPointX>0.5</sld:AnchorPointX>
                                    <sld:AnchorPointY>0.5</sld:AnchorPointY>
                                </sld:AnchorPoint>
                                <sld:Displacement>
                                    <sld:DisplacementX>0.0</sld:DisplacementX>
                                    <sld:DisplacementY>0.0</sld:DisplacementY>
                                </sld:Displacement>
                            </sld:PointPlacement>
                        </sld:LabelPlacement>
                        <sld:Halo>
                            <sld:Radius>1.0</sld:Radius>
                            <sld:Fill>
                                <sld:CssParameter name="fill">#FFFFFF</sld:CssParameter>
                                <sld:CssParameter name="fill-opacity">0.9</sld:CssParameter>
                            </sld:Fill>
                        </sld:Halo>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#000000</sld:CssParameter>
                        </sld:Fill>
                        <sld:VendorOption name="autoWrap">70</sld:VendorOption>
                        <sld:VendorOption name="group">true</sld:VendorOption>
                        <sld:VendorOption name="spaceAround">30</sld:VendorOption>
                    </sld:TextSymbolizer>
                </sld:Rule>
            </sld:FeatureTypeStyle>
        </sld:UserStyle>
        <sld:UserStyle>
            <sld:Name>provincias-gris</sld:Name>
            <sld:Title>Estilo de provincias para EGEO</sld:Title>
            <sld:Abstract>Se representan polígonos y etiquetas de Provincias.</sld:Abstract>
            <sld:FeatureTypeStyle>
                <sld:Name>name</sld:Name>
                <sld:Rule>
                    <sld:Name>poligono_provincias</sld:Name>
                    <sld:Title>Provincias</sld:Title>
                    <sld:Abstract>Provincias con relleno gris #EDEBE4 y borde discontinuo gris #9F9A91 de 0.4 píxel.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                            <ogc:PropertyName>nationallevel</ogc:PropertyName>
                            <ogc:Literal>%3rdOrder</ogc:Literal>
                        </ogc:PropertyIsLike>
                    </ogc:Filter>
                    <sld:PolygonSymbolizer>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#EDEBE4</sld:CssParameter>
                        </sld:Fill>
                        <sld:Stroke>
                            <sld:CssParameter name="stroke">#9F9A91</sld:CssParameter>
                            <sld:CssParameter name="stroke-linecap">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-linejoin">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-width">0.4</sld:CssParameter>
                            <sld:CssParameter name="stroke-dasharray">2.5 2.5</sld:CssParameter>
                        </sld:Stroke>
                    </sld:PolygonSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>eti_España</sld:Name>
                    <sld:Title>Texto España</sld:Title>
                    <sld:Abstract>Arial 12 negrita.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                            <ogc:PropertyName>nationallevel</ogc:PropertyName>
                            <ogc:Literal>%1stOrder</ogc:Literal>
                        </ogc:PropertyIsLike>
                    </ogc:Filter>
                    <sld:MinScaleDenominator>1.0E7</sld:MinScaleDenominator>
                    <sld:TextSymbolizer>
                        <sld:Label>
                            <ogc:PropertyName>nameunit</ogc:PropertyName>
                        </sld:Label>
                        <sld:Font>
                            <sld:CssParameter name="font-family">Arial</sld:CssParameter>
                            <sld:CssParameter name="font-size">12</sld:CssParameter>
                            <sld:CssParameter name="font-style">normal</sld:CssParameter>
                            <sld:CssParameter name="font-weight">bold</sld:CssParameter>
                        </sld:Font>
                        <sld:LabelPlacement>
                            <sld:PointPlacement>
                                <sld:AnchorPoint>
                                    <sld:AnchorPointX>0.0</sld:AnchorPointX>
                                    <sld:AnchorPointY>0.0</sld:AnchorPointY>
                                </sld:AnchorPoint>
                                <sld:Displacement>
                                    <sld:DisplacementX>0.0</sld:DisplacementX>
                                    <sld:DisplacementY>0.0</sld:DisplacementY>
                                </sld:Displacement>
                            </sld:PointPlacement>
                        </sld:LabelPlacement>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#262526</sld:CssParameter>
                        </sld:Fill>
                        <sld:VendorOption name="autoWrap">85</sld:VendorOption>
                        <sld:VendorOption name="group">true</sld:VendorOption>
                        <sld:VendorOption name="spaceAround">5</sld:VendorOption>
                    </sld:TextSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>eti_provincias</sld:Name>
                    <sld:Title>Etiquetas de Provincias</sld:Title>
                    <sld:Abstract>Arial 11 normal.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:And>
                            <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                                <ogc:PropertyName>nationallevel</ogc:PropertyName>
                                <ogc:Literal>%3rdOrder</ogc:Literal>
                            </ogc:PropertyIsLike>
                            <ogc:Not>
                                <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                                    <ogc:PropertyName>nameunit</ogc:PropertyName>
                                    <ogc:Literal>Conjurisdicciones%</ogc:Literal>
                                </ogc:PropertyIsLike>
                            </ogc:Not>
                        </ogc:And>
                    </ogc:Filter>
                    <sld:MaxScaleDenominator>1.0E7</sld:MaxScaleDenominator>
                    <sld:TextSymbolizer>
                        <sld:Label>
                            <ogc:PropertyName>nameunit</ogc:PropertyName>
                        </sld:Label>
                        <sld:Font>
                            <sld:CssParameter name="font-family">Arial</sld:CssParameter>
                            <sld:CssParameter name="font-size">11</sld:CssParameter>
                            <sld:CssParameter name="font-style">normal</sld:CssParameter>
                            <sld:CssParameter name="font-weight">normal</sld:CssParameter>
                        </sld:Font>
                        <sld:LabelPlacement>
                            <sld:PointPlacement>
                                <sld:AnchorPoint>
                                    <sld:AnchorPointX>0.5</sld:AnchorPointX>
                                    <sld:AnchorPointY>0.5</sld:AnchorPointY>
                                </sld:AnchorPoint>
                                <sld:Displacement>
                                    <sld:DisplacementX>0.0</sld:DisplacementX>
                                    <sld:DisplacementY>0.0</sld:DisplacementY>
                                </sld:Displacement>
                            </sld:PointPlacement>
                        </sld:LabelPlacement>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#000000</sld:CssParameter>
                        </sld:Fill>
                        <sld:VendorOption name="autoWrap">85</sld:VendorOption>
                        <sld:VendorOption name="group">true</sld:VendorOption>
                        <sld:VendorOption name="spaceAround">5</sld:VendorOption>
                    </sld:TextSymbolizer>
                </sld:Rule>
            </sld:FeatureTypeStyle>
        </sld:UserStyle>
        <sld:UserStyle>
            <sld:Name>ua-comparador</sld:Name>
            <sld:Title>IGN: Estilo de unidadeas administrativas</sld:Title>
            <sld:Abstract>Estilo de representación del IGN de unidades administrativas.</sld:Abstract>
            <sld:FeatureTypeStyle>
                <sld:Name>UnidadesAdministrativas</sld:Name>
                <sld:Title>IGN: Estilo de Comunidades Autonómas</sld:Title>
                <sld:Abstract>Estilo de representación del IGN de unidades administrativas.</sld:Abstract>
                <sld:Rule>
                    <sld:Name>ComunidadAutonoma</sld:Name>
                    <sld:Abstract>Estilo de representación de límite administrativo de Comunidades Autonómas.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                            <ogc:PropertyName>nationallevel</ogc:PropertyName>
                            <ogc:Literal>%2ndOrder</ogc:Literal>
                        </ogc:PropertyIsLike>
                    </ogc:Filter>
                    <sld:MinScaleDenominator>5000000.0</sld:MinScaleDenominator>
                    <sld:PolygonSymbolizer>
                        <sld:Stroke>
                            <sld:CssParameter name="stroke">#a50f15</sld:CssParameter>
                            <sld:CssParameter name="stroke-linecap">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-linejoin">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-opacity">0.8</sld:CssParameter>
                        </sld:Stroke>
                    </sld:PolygonSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>ComunidadAutonoma</sld:Name>
                    <sld:Abstract>Estilo de representación de límite administrativo de Comunidades Autonómas.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                            <ogc:PropertyName>nationallevel</ogc:PropertyName>
                            <ogc:Literal>%2ndOrder</ogc:Literal>
                        </ogc:PropertyIsLike>
                    </ogc:Filter>
                    <sld:MaxScaleDenominator>5000000.0</sld:MaxScaleDenominator>
                    <sld:PolygonSymbolizer>
                        <sld:Stroke>
                            <sld:CssParameter name="stroke">#a50f15</sld:CssParameter>
                            <sld:CssParameter name="stroke-linecap">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-linejoin">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-opacity">0.8</sld:CssParameter>
                            <sld:CssParameter name="stroke-width">2.0</sld:CssParameter>
                        </sld:Stroke>
                    </sld:PolygonSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>ETI_COMUNIDADES_AUTONOMAS</sld:Name>
                    <sld:Abstract>Rotulación  de etiquetas de CCAA entre las escalas 1:15M y 1:10M. Tamaño de letra 6  y no se etiquetan las conjurisdicciones autonómicas</sld:Abstract>
                    <ogc:Filter>
                        <ogc:And>
                            <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                                <ogc:PropertyName>nationallevel</ogc:PropertyName>
                                <ogc:Literal>%2ndOrder</ogc:Literal>
                            </ogc:PropertyIsLike>
                            <ogc:Not>
                                <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                                    <ogc:PropertyName>nameunit</ogc:PropertyName>
                                    <ogc:Literal>Conjurisdicciones%</ogc:Literal>
                                </ogc:PropertyIsLike>
                            </ogc:Not>
                        </ogc:And>
                    </ogc:Filter>
                    <sld:MinScaleDenominator>1.0E7</sld:MinScaleDenominator>
                    <sld:MaxScaleDenominator>1.5E7</sld:MaxScaleDenominator>
                    <sld:TextSymbolizer>
                        <sld:Label>
                            <ogc:PropertyName>nameunit</ogc:PropertyName>
                        </sld:Label>
                        <sld:Font>
                            <sld:CssParameter name="font-family">Arial</sld:CssParameter>
                            <sld:CssParameter name="font-size">50.0</sld:CssParameter>
                            <sld:CssParameter name="font-style">normal</sld:CssParameter>
                            <sld:CssParameter name="font-weight">normal</sld:CssParameter>
                        </sld:Font>
                        <sld:LabelPlacement>
                            <sld:PointPlacement>
                                <sld:AnchorPoint>
                                    <sld:AnchorPointX>0.5</sld:AnchorPointX>
                                    <sld:AnchorPointY>0.5</sld:AnchorPointY>
                                </sld:AnchorPoint>
                                <sld:Displacement>
                                    <sld:DisplacementX>0.0</sld:DisplacementX>
                                    <sld:DisplacementY>0.0</sld:DisplacementY>
                                </sld:Displacement>
                            </sld:PointPlacement>
                        </sld:LabelPlacement>
                        <sld:Halo>
                            <sld:Radius>0.5</sld:Radius>
                            <sld:Fill>
                                <sld:CssParameter name="fill">#FFFFFF</sld:CssParameter>
                            </sld:Fill>
                        </sld:Halo>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#000000</sld:CssParameter>
                        </sld:Fill>
                        <sld:VendorOption name="group">true</sld:VendorOption>
                        <sld:VendorOption name="autoWrap">70</sld:VendorOption>
                        <sld:VendorOption name="goodnessOfFit">0.0</sld:VendorOption>
                    </sld:TextSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>ETI_COMUNIDADES_AUTONOMAS</sld:Name>
                    <sld:Abstract>Rotulación  de etiquetas de CCAA entre las escalas 1:10M y 1:5M. Tamaño de letra 8 y  no se etiquetan las conjurisdicciones autonómicas</sld:Abstract>
                    <ogc:Filter>
                        <ogc:And>
                            <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                                <ogc:PropertyName>nationallevel</ogc:PropertyName>
                                <ogc:Literal>%2ndOrder</ogc:Literal>
                            </ogc:PropertyIsLike>
                            <ogc:Not>
                                <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                                    <ogc:PropertyName>nameunit</ogc:PropertyName>
                                    <ogc:Literal>Conjurisdicciones%</ogc:Literal>
                                </ogc:PropertyIsLike>
                            </ogc:Not>
                        </ogc:And>
                    </ogc:Filter>
                    <sld:MinScaleDenominator>5000000.0</sld:MinScaleDenominator>
                    <sld:MaxScaleDenominator>1.0E7</sld:MaxScaleDenominator>
                    <sld:TextSymbolizer>
                        <sld:Label>
                            <ogc:PropertyName>nameunit</ogc:PropertyName>
                        </sld:Label>
                        <sld:Font>
                            <sld:CssParameter name="font-family">Arial</sld:CssParameter>
                            <sld:CssParameter name="font-size">50.0</sld:CssParameter>
                            <sld:CssParameter name="font-style">normal</sld:CssParameter>
                            <sld:CssParameter name="font-weight">normal</sld:CssParameter>
                        </sld:Font>
                        <sld:LabelPlacement>
                            <sld:PointPlacement>
                                <sld:AnchorPoint>
                                    <sld:AnchorPointX>0.5</sld:AnchorPointX>
                                    <sld:AnchorPointY>0.5</sld:AnchorPointY>
                                </sld:AnchorPoint>
                                <sld:Displacement>
                                    <sld:DisplacementX>0.0</sld:DisplacementX>
                                    <sld:DisplacementY>0.0</sld:DisplacementY>
                                </sld:Displacement>
                            </sld:PointPlacement>
                        </sld:LabelPlacement>
                        <sld:Halo>
                            <sld:Radius>1.0</sld:Radius>
                            <sld:Fill>
                                <sld:CssParameter name="fill">#FFFFFF</sld:CssParameter>
                                <sld:CssParameter name="fill-opacity">0.9</sld:CssParameter>
                            </sld:Fill>
                        </sld:Halo>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#000000</sld:CssParameter>
                        </sld:Fill>
                        <sld:VendorOption name="group">true</sld:VendorOption>
                        <sld:VendorOption name="autoWrap">70</sld:VendorOption>
                        <sld:VendorOption name="goodnessOfFit">0.0</sld:VendorOption>
                        <sld:VendorOption name="maxDisplacement">300</sld:VendorOption>
                    </sld:TextSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>PROVINCIAS</sld:Name>
                    <sld:Abstract>Estilo de representación de los límites administrativos provinciales.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                            <ogc:PropertyName>nationallevel</ogc:PropertyName>
                            <ogc:Literal>%3rdOrder</ogc:Literal>
                        </ogc:PropertyIsLike>
                    </ogc:Filter>
                    <sld:PolygonSymbolizer>
                        <sld:Stroke>
                            <sld:CssParameter name="stroke">#de2d26</sld:CssParameter>
                            <sld:CssParameter name="stroke-linecap">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-linejoin">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-opacity">0.8</sld:CssParameter>
                        </sld:Stroke>
                    </sld:PolygonSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>ETI_PROVINCIAS</sld:Name>
                    <sld:Abstract>Escala  entre 1:5M y 1:2M. Estilo de representación de las etiquetas relativas a  los límites administrativos provinciales.No se etiquetan las  conjurisdicciones</sld:Abstract>
                    <ogc:Filter>
                        <ogc:And>
                            <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                                <ogc:PropertyName>nationallevel</ogc:PropertyName>
                                <ogc:Literal>%3rdOrder</ogc:Literal>
                            </ogc:PropertyIsLike>
                            <ogc:Not>
                                <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                                    <ogc:PropertyName>nameunit</ogc:PropertyName>
                                    <ogc:Literal>Conjurisdicciones%</ogc:Literal>
                                </ogc:PropertyIsLike>
                            </ogc:Not>
                        </ogc:And>
                    </ogc:Filter>
                    <sld:MinScaleDenominator>2000000.0</sld:MinScaleDenominator>
                    <sld:MaxScaleDenominator>5000000.0</sld:MaxScaleDenominator>
                    <sld:TextSymbolizer>
                        <sld:Label>
                            <ogc:PropertyName>nameunit</ogc:PropertyName>
                        </sld:Label>
                        <sld:Font>
                            <sld:CssParameter name="font-family">Arial</sld:CssParameter>
                            <sld:CssParameter name="font-size">50.0</sld:CssParameter>
                            <sld:CssParameter name="font-style">normal</sld:CssParameter>
                            <sld:CssParameter name="font-weight">normal</sld:CssParameter>
                        </sld:Font>
                        <sld:LabelPlacement>
                            <sld:PointPlacement>
                                <sld:AnchorPoint>
                                    <sld:AnchorPointX>0.5</sld:AnchorPointX>
                                    <sld:AnchorPointY>0.5</sld:AnchorPointY>
                                </sld:AnchorPoint>
                                <sld:Displacement>
                                    <sld:DisplacementX>0.0</sld:DisplacementX>
                                    <sld:DisplacementY>0.0</sld:DisplacementY>
                                </sld:Displacement>
                            </sld:PointPlacement>
                        </sld:LabelPlacement>
                        <sld:Halo>
                            <sld:Radius>2.0</sld:Radius>
                            <sld:Fill>
                                <sld:CssParameter name="fill">#FFFFFF</sld:CssParameter>
                                <sld:CssParameter name="fill-opacity">0.9</sld:CssParameter>
                            </sld:Fill>
                        </sld:Halo>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#000000</sld:CssParameter>
                        </sld:Fill>
                        <sld:Priority>
                            <ogc:PropertyName>nameunit</ogc:PropertyName>
                        </sld:Priority>
                        <sld:VendorOption name="goodnessOfFit">0.0</sld:VendorOption>
                        <sld:VendorOption name="group">true</sld:VendorOption>
                        <sld:VendorOption name="autoWrap">70</sld:VendorOption>
                    </sld:TextSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>ETI_PROVINCIAS</sld:Name>
                    <sld:Abstract>Escala  entre 1:2M y 1:0.8M. Estilo de representación de las etiquetas  relativas a los límites administrativos provinciales.Sí se etiquetan las  conjurisdicciones</sld:Abstract>
                    <ogc:Filter>
                        <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                            <ogc:PropertyName>nationallevel</ogc:PropertyName>
                            <ogc:Literal>%3rdOrder</ogc:Literal>
                        </ogc:PropertyIsLike>
                    </ogc:Filter>
                    <sld:MinScaleDenominator>800000.0</sld:MinScaleDenominator>
                    <sld:MaxScaleDenominator>2000000.0</sld:MaxScaleDenominator>
                    <sld:TextSymbolizer>
                        <sld:Label>
                            <ogc:PropertyName>nameunit</ogc:PropertyName>
                        </sld:Label>
                        <sld:Font>
                            <sld:CssParameter name="font-family">Arial</sld:CssParameter>
                            <sld:CssParameter name="font-size">11.0</sld:CssParameter>
                            <sld:CssParameter name="font-style">normal</sld:CssParameter>
                            <sld:CssParameter name="font-weight">normal</sld:CssParameter>
                        </sld:Font>
                        <sld:LabelPlacement>
                            <sld:PointPlacement>
                                <sld:AnchorPoint>
                                    <sld:AnchorPointX>0.5</sld:AnchorPointX>
                                    <sld:AnchorPointY>0.5</sld:AnchorPointY>
                                </sld:AnchorPoint>
                                <sld:Displacement>
                                    <sld:DisplacementX>0.0</sld:DisplacementX>
                                    <sld:DisplacementY>0.0</sld:DisplacementY>
                                </sld:Displacement>
                            </sld:PointPlacement>
                        </sld:LabelPlacement>
                        <sld:Halo>
                            <sld:Radius>1.0</sld:Radius>
                            <sld:Fill>
                                <sld:CssParameter name="fill">#FFFFFF</sld:CssParameter>
                                <sld:CssParameter name="fill-opacity">0.9</sld:CssParameter>
                            </sld:Fill>
                        </sld:Halo>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#000000</sld:CssParameter>
                        </sld:Fill>
                        <sld:Priority>
                            <ogc:PropertyName>nameunit</ogc:PropertyName>
                        </sld:Priority>
                        <sld:VendorOption name="goodnessOfFit">0.0</sld:VendorOption>
                        <sld:VendorOption name="group">true</sld:VendorOption>
                        <sld:VendorOption name="autoWrap">70</sld:VendorOption>
                    </sld:TextSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>Municipios</sld:Name>
                    <sld:Title>Municipios</sld:Title>
                    <sld:Abstract>Estilo de representaci?n de los l?mites administrativos municipales.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                            <ogc:PropertyName>nationallevel</ogc:PropertyName>
                            <ogc:Literal>%4thOrder</ogc:Literal>
                        </ogc:PropertyIsLike>
                    </ogc:Filter>
                    <sld:MaxScaleDenominator>1000000.0</sld:MaxScaleDenominator>
                    <sld:PolygonSymbolizer>
                        <sld:Stroke>
                            <sld:CssParameter name="stroke">#fb6a4a</sld:CssParameter>
                            <sld:CssParameter name="stroke-linecap">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-linejoin">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-width">0.3</sld:CssParameter>
                        </sld:Stroke>
                    </sld:PolygonSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>ETI_MUNICIPIOS</sld:Name>
                    <sld:Abstract>Estilo de representación de las etiquetas relativas a los límites administrativos municipales.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                            <ogc:PropertyName>nationallevel</ogc:PropertyName>
                            <ogc:Literal>%4thOrder</ogc:Literal>
                        </ogc:PropertyIsLike>
                    </ogc:Filter>
                    <sld:MaxScaleDenominator>1000000.0</sld:MaxScaleDenominator>
                    <sld:TextSymbolizer>
                        <sld:Label>
                            <ogc:PropertyName>nameunit</ogc:PropertyName>
                        </sld:Label>
                        <sld:Font>
                            <sld:CssParameter name="font-family">Arial</sld:CssParameter>
                            <sld:CssParameter name="font-size">11.0</sld:CssParameter>
                            <sld:CssParameter name="font-style">normal</sld:CssParameter>
                            <sld:CssParameter name="font-weight">normal</sld:CssParameter>
                        </sld:Font>
                        <sld:LabelPlacement>
                            <sld:PointPlacement>
                                <sld:AnchorPoint>
                                    <sld:AnchorPointX>0.5</sld:AnchorPointX>
                                    <sld:AnchorPointY>0.5</sld:AnchorPointY>
                                </sld:AnchorPoint>
                                <sld:Displacement>
                                    <sld:DisplacementX>0.0</sld:DisplacementX>
                                    <sld:DisplacementY>0.0</sld:DisplacementY>
                                </sld:Displacement>
                            </sld:PointPlacement>
                        </sld:LabelPlacement>
                        <sld:Halo>
                            <sld:Radius>1.0</sld:Radius>
                            <sld:Fill>
                                <sld:CssParameter name="fill">#FFFFFF</sld:CssParameter>
                                <sld:CssParameter name="fill-opacity">0.9</sld:CssParameter>
                            </sld:Fill>
                        </sld:Halo>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#000000</sld:CssParameter>
                        </sld:Fill>
                        <sld:VendorOption name="autoWrap">70</sld:VendorOption>
                        <sld:VendorOption name="group">true</sld:VendorOption>
                        <sld:VendorOption name="spaceAround">30</sld:VendorOption>
                    </sld:TextSymbolizer>
                </sld:Rule>
            </sld:FeatureTypeStyle>
        </sld:UserStyle>
        <sld:UserStyle>
            <sld:Name>municipios-gris</sld:Name>
            <sld:Title>Estilo de municipios para EGEO</sld:Title>
            <sld:Abstract>Se representan polígonos y etiquetas de Municipios.</sld:Abstract>
            <sld:FeatureTypeStyle>
                <sld:Name>name</sld:Name>
                <sld:Rule>
                    <sld:Name>poligono_provincias</sld:Name>
                    <sld:Title>Provincias</sld:Title>
                    <sld:Abstract>Provincias con relleno gris #EDEBE4 y borde continuo gris #9F9A91 de 0.4 píxel.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                            <ogc:PropertyName>nationallevel</ogc:PropertyName>
                            <ogc:Literal>%3rdOrder</ogc:Literal>
                        </ogc:PropertyIsLike>
                    </ogc:Filter>
                    <sld:MinScaleDenominator>2500000.0</sld:MinScaleDenominator>
                    <sld:PolygonSymbolizer>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#EDEBE4</sld:CssParameter>
                        </sld:Fill>
                        <sld:Stroke>
                            <sld:CssParameter name="stroke">#9F9A91</sld:CssParameter>
                            <sld:CssParameter name="stroke-linecap">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-linejoin">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-width">0.4</sld:CssParameter>
                        </sld:Stroke>
                    </sld:PolygonSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>poligono_municipios</sld:Name>
                    <sld:Title>Municipios</sld:Title>
                    <sld:Abstract>Municipios con relleno gris #EDEBE4 y borde discontinuo gris #9F9A91 de 0.2 píxel.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                            <ogc:PropertyName>nationallevel</ogc:PropertyName>
                            <ogc:Literal>%4thOrder</ogc:Literal>
                        </ogc:PropertyIsLike>
                    </ogc:Filter>
                    <sld:MaxScaleDenominator>2500000.0</sld:MaxScaleDenominator>
                    <sld:PolygonSymbolizer>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#EDEBE4</sld:CssParameter>
                        </sld:Fill>
                        <sld:Stroke>
                            <sld:CssParameter name="stroke">#9F9A91</sld:CssParameter>
                            <sld:CssParameter name="stroke-linecap">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-linejoin">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-width">0.2</sld:CssParameter>
                            <sld:CssParameter name="stroke-dasharray">2.0 2.0</sld:CssParameter>
                        </sld:Stroke>
                    </sld:PolygonSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>eti_España</sld:Name>
                    <sld:Title>Texto España</sld:Title>
                    <sld:Abstract>Arial 12 negrita.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                            <ogc:PropertyName>nationallevel</ogc:PropertyName>
                            <ogc:Literal>%1stOrder</ogc:Literal>
                        </ogc:PropertyIsLike>
                    </ogc:Filter>
                    <sld:MinScaleDenominator>1000000.0</sld:MinScaleDenominator>
                    <sld:TextSymbolizer>
                        <sld:Label>
                            <ogc:PropertyName>nameunit</ogc:PropertyName>
                        </sld:Label>
                        <sld:Font>
                            <sld:CssParameter name="font-family">Arial</sld:CssParameter>
                            <sld:CssParameter name="font-size">12</sld:CssParameter>
                            <sld:CssParameter name="font-style">normal</sld:CssParameter>
                            <sld:CssParameter name="font-weight">bold</sld:CssParameter>
                        </sld:Font>
                        <sld:LabelPlacement>
                            <sld:PointPlacement>
                                <sld:AnchorPoint>
                                    <sld:AnchorPointX>0.0</sld:AnchorPointX>
                                    <sld:AnchorPointY>0.0</sld:AnchorPointY>
                                </sld:AnchorPoint>
                                <sld:Displacement>
                                    <sld:DisplacementX>0.0</sld:DisplacementX>
                                    <sld:DisplacementY>0.0</sld:DisplacementY>
                                </sld:Displacement>
                            </sld:PointPlacement>
                        </sld:LabelPlacement>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#262526</sld:CssParameter>
                        </sld:Fill>
                        <sld:VendorOption name="autoWrap">85</sld:VendorOption>
                        <sld:VendorOption name="group">true</sld:VendorOption>
                        <sld:VendorOption name="spaceAround">5</sld:VendorOption>
                    </sld:TextSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>eti_provincias</sld:Name>
                    <sld:Title>Etiquetas de Provincias</sld:Title>
                    <sld:Abstract>Arial 12 normal.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                            <ogc:PropertyName>nationallevel</ogc:PropertyName>
                            <ogc:Literal>%3rdOrder</ogc:Literal>
                        </ogc:PropertyIsLike>
                    </ogc:Filter>
                    <sld:MinScaleDenominator>1000000.0</sld:MinScaleDenominator>
                    <sld:MaxScaleDenominator>2500000.0</sld:MaxScaleDenominator>
                    <sld:TextSymbolizer>
                        <sld:Label>
                            <ogc:PropertyName>nameunit</ogc:PropertyName>
                        </sld:Label>
                        <sld:Font>
                            <sld:CssParameter name="font-family">Arial</sld:CssParameter>
                            <sld:CssParameter name="font-size">12</sld:CssParameter>
                            <sld:CssParameter name="font-style">normal</sld:CssParameter>
                            <sld:CssParameter name="font-weight">normal</sld:CssParameter>
                        </sld:Font>
                        <sld:LabelPlacement>
                            <sld:PointPlacement>
                                <sld:AnchorPoint>
                                    <sld:AnchorPointX>0.0</sld:AnchorPointX>
                                    <sld:AnchorPointY>0.0</sld:AnchorPointY>
                                </sld:AnchorPoint>
                                <sld:Displacement>
                                    <sld:DisplacementX>0.0</sld:DisplacementX>
                                    <sld:DisplacementY>0.0</sld:DisplacementY>
                                </sld:Displacement>
                            </sld:PointPlacement>
                        </sld:LabelPlacement>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#262526</sld:CssParameter>
                        </sld:Fill>
                        <sld:VendorOption name="autoWrap">85</sld:VendorOption>
                        <sld:VendorOption name="group">true</sld:VendorOption>
                        <sld:VendorOption name="spaceAround">5</sld:VendorOption>
                    </sld:TextSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>eti_municipios</sld:Name>
                    <sld:Title>Etiquetas de Municipios</sld:Title>
                    <sld:Abstract>Arial 11 normal.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:And>
                            <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                                <ogc:PropertyName>nationallevel</ogc:PropertyName>
                                <ogc:Literal>%4thOrder</ogc:Literal>
                            </ogc:PropertyIsLike>
                            <ogc:Not>
                                <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                                    <ogc:PropertyName>nameunit</ogc:PropertyName>
                                    <ogc:Literal>Conjurisdicciones%</ogc:Literal>
                                </ogc:PropertyIsLike>
                            </ogc:Not>
                        </ogc:And>
                    </ogc:Filter>
                    <sld:MaxScaleDenominator>2500000.0</sld:MaxScaleDenominator>
                    <sld:TextSymbolizer>
                        <sld:Label>
                            <ogc:PropertyName>nameunit</ogc:PropertyName>
                        </sld:Label>
                        <sld:Font>
                            <sld:CssParameter name="font-family">Arial</sld:CssParameter>
                            <sld:CssParameter name="font-size">11</sld:CssParameter>
                            <sld:CssParameter name="font-style">normal</sld:CssParameter>
                            <sld:CssParameter name="font-weight">normal</sld:CssParameter>
                        </sld:Font>
                        <sld:LabelPlacement>
                            <sld:PointPlacement>
                                <sld:AnchorPoint>
                                    <sld:AnchorPointX>0.5</sld:AnchorPointX>
                                    <sld:AnchorPointY>0.5</sld:AnchorPointY>
                                </sld:AnchorPoint>
                                <sld:Displacement>
                                    <sld:DisplacementX>0.0</sld:DisplacementX>
                                    <sld:DisplacementY>0.0</sld:DisplacementY>
                                </sld:Displacement>
                            </sld:PointPlacement>
                        </sld:LabelPlacement>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#000000</sld:CssParameter>
                        </sld:Fill>
                        <sld:VendorOption name="autoWrap">85</sld:VendorOption>
                        <sld:VendorOption name="group">true</sld:VendorOption>
                        <sld:VendorOption name="spaceAround">5</sld:VendorOption>
                    </sld:TextSymbolizer>
                </sld:Rule>
            </sld:FeatureTypeStyle>
            <sld:FeatureTypeStyle>
                <sld:Name>name</sld:Name>
                <sld:Rule>
                    <sld:Name>contorno_provincias</sld:Name>
                    <sld:Title>Provincias</sld:Title>
                    <sld:Abstract>Provincias con borde continuo gris #9F9A91 de 0.5 píxel.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                            <ogc:PropertyName>nationallevel</ogc:PropertyName>
                            <ogc:Literal>%3rdOrder</ogc:Literal>
                        </ogc:PropertyIsLike>
                    </ogc:Filter>
                    <sld:MaxScaleDenominator>2500000.0</sld:MaxScaleDenominator>
                    <sld:PolygonSymbolizer>
                        <sld:Stroke>
                            <sld:CssParameter name="stroke">#9F9A91</sld:CssParameter>
                            <sld:CssParameter name="stroke-linecap">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-linejoin">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-width">0.5</sld:CssParameter>
                        </sld:Stroke>
                    </sld:PolygonSymbolizer>
                </sld:Rule>
            </sld:FeatureTypeStyle>
        </sld:UserStyle>
        <sld:UserStyle>
            <sld:Name>ua-EscalaGris</sld:Name>
            <sld:Title>IGN: Estilo de unidadeas administrativas</sld:Title>
            <sld:Abstract>Estilo de representación para Cartociudad de unidades administrativas.</sld:Abstract>
            <sld:FeatureTypeStyle>
                <sld:Name>UnidadesAdministrativas</sld:Name>
                <sld:Title>IGN: Estilo de Comunidades Autonómas</sld:Title>
                <sld:Abstract>Estilo de representación del IGN de unidades administrativas.</sld:Abstract>
                <sld:Rule>
                    <sld:Name>ComunidadAutonoma</sld:Name>
                    <sld:Abstract>Estilo de representación de límite administrativo de Comunidades Autonómas.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                            <ogc:PropertyName>nationallevel</ogc:PropertyName>
                            <ogc:Literal>%2ndOrder</ogc:Literal>
                        </ogc:PropertyIsLike>
                    </ogc:Filter>
                    <sld:MinScaleDenominator>5000000.0</sld:MinScaleDenominator>
                    <sld:PolygonSymbolizer>
                        <sld:Stroke>
                            <sld:CssParameter name="stroke">#797977</sld:CssParameter>
                            <sld:CssParameter name="stroke-linecap">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-linejoin">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-width">0.8</sld:CssParameter>
                        </sld:Stroke>
                    </sld:PolygonSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>ComunidadAutonoma</sld:Name>
                    <sld:Abstract>Estilo de representación de límite administrativo de Comunidades Autonómas.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                            <ogc:PropertyName>nationallevel</ogc:PropertyName>
                            <ogc:Literal>%2ndOrder</ogc:Literal>
                        </ogc:PropertyIsLike>
                    </ogc:Filter>
                    <sld:MaxScaleDenominator>5000000.0</sld:MaxScaleDenominator>
                    <sld:PolygonSymbolizer>
                        <sld:Stroke>
                            <sld:CssParameter name="stroke">#797977</sld:CssParameter>
                            <sld:CssParameter name="stroke-linecap">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-linejoin">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-width">0.8</sld:CssParameter>
                        </sld:Stroke>
                    </sld:PolygonSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>Provincias</sld:Name>
                    <sld:Abstract>Estilo de representación de los límites administrativos provinciales.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                            <ogc:PropertyName>nationallevel</ogc:PropertyName>
                            <ogc:Literal>%3rdOrder</ogc:Literal>
                        </ogc:PropertyIsLike>
                    </ogc:Filter>
                    <sld:MaxScaleDenominator>5000000.0</sld:MaxScaleDenominator>
                    <sld:PolygonSymbolizer>
                        <sld:Stroke>
                            <sld:CssParameter name="stroke">#A7A7A6</sld:CssParameter>
                            <sld:CssParameter name="stroke-linecap">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-linejoin">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-opacity">0.9</sld:CssParameter>
                            <sld:CssParameter name="stroke-width">0.5</sld:CssParameter>
                            <sld:CssParameter name="stroke-dasharray">4.0 4.0</sld:CssParameter>
                        </sld:Stroke>
                    </sld:PolygonSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>Municipios</sld:Name>
                    <sld:Title>Municipios</sld:Title>
                    <sld:Abstract>Estilo de representaci?n de los l?mites administrativos municipales.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                            <ogc:PropertyName>nationallevel</ogc:PropertyName>
                            <ogc:Literal>%4thOrder</ogc:Literal>
                        </ogc:PropertyIsLike>
                    </ogc:Filter>
                    <sld:MaxScaleDenominator>600000.0</sld:MaxScaleDenominator>
                    <sld:PolygonSymbolizer>
                        <sld:Stroke>
                            <sld:CssParameter name="stroke">#A7A7A6</sld:CssParameter>
                            <sld:CssParameter name="stroke-linecap">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-linejoin">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-opacity">0.9</sld:CssParameter>
                            <sld:CssParameter name="stroke-width">0.2</sld:CssParameter>
                            <sld:CssParameter name="stroke-dasharray">4.0 1.0 4.0 1.0 4.0</sld:CssParameter>
                        </sld:Stroke>
                    </sld:PolygonSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>ETI_MUNICIPIOS</sld:Name>
                    <sld:Abstract>Estilo de representación de las etiquetas relativas a los límites administrativos municipales.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                            <ogc:PropertyName>nationallevel</ogc:PropertyName>
                            <ogc:Literal>%4thOrder</ogc:Literal>
                        </ogc:PropertyIsLike>
                    </ogc:Filter>
                    <sld:MaxScaleDenominator>50000.0</sld:MaxScaleDenominator>
                    <sld:TextSymbolizer>
                        <sld:Label>
                            <ogc:PropertyName>nameunit</ogc:PropertyName>
                        </sld:Label>
                        <sld:Font>
                            <sld:CssParameter name="font-family">Arial</sld:CssParameter>
                            <sld:CssParameter name="font-size">11.0</sld:CssParameter>
                            <sld:CssParameter name="font-style">normal</sld:CssParameter>
                            <sld:CssParameter name="font-weight">normal</sld:CssParameter>
                        </sld:Font>
                        <sld:LabelPlacement>
                            <sld:PointPlacement>
                                <sld:AnchorPoint>
                                    <sld:AnchorPointX>0.5</sld:AnchorPointX>
                                    <sld:AnchorPointY>0.5</sld:AnchorPointY>
                                </sld:AnchorPoint>
                                <sld:Displacement>
                                    <sld:DisplacementX>0.0</sld:DisplacementX>
                                    <sld:DisplacementY>0.0</sld:DisplacementY>
                                </sld:Displacement>
                            </sld:PointPlacement>
                        </sld:LabelPlacement>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#818180</sld:CssParameter>
                        </sld:Fill>
                        <sld:VendorOption name="autoWrap">85</sld:VendorOption>
                        <sld:VendorOption name="group">true</sld:VendorOption>
                        <sld:VendorOption name="spaceAround">5</sld:VendorOption>
                    </sld:TextSymbolizer>
                </sld:Rule>
            </sld:FeatureTypeStyle>
        </sld:UserStyle>
        <sld:UserStyle>
            <sld:Name>ccaa-gris</sld:Name>
            <sld:Title>Estilo de ccaa para EGEO</sld:Title>
            <sld:Abstract>Se representan polígonos y etiquetas de Comunidades Autónomas.</sld:Abstract>
            <sld:FeatureTypeStyle>
                <sld:Name>name</sld:Name>
                <sld:Rule>
                    <sld:Name>poligono_ccaa</sld:Name>
                    <sld:Title>Comunidades Autónomas</sld:Title>
                    <sld:Abstract>CCAA con relleno gris #EDEBE4 y borde discontinuo gris #9F9A91 de 0.5 píxel.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                            <ogc:PropertyName>nationallevel</ogc:PropertyName>
                            <ogc:Literal>%2ndOrder</ogc:Literal>
                        </ogc:PropertyIsLike>
                    </ogc:Filter>
                    <sld:PolygonSymbolizer>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#EDEBE4</sld:CssParameter>
                        </sld:Fill>
                        <sld:Stroke>
                            <sld:CssParameter name="stroke">#9F9A91</sld:CssParameter>
                            <sld:CssParameter name="stroke-linecap">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-linejoin">round</sld:CssParameter>
                            <sld:CssParameter name="stroke-width">0.5</sld:CssParameter>
                            <sld:CssParameter name="stroke-dasharray">3.5 3.5</sld:CssParameter>
                        </sld:Stroke>
                    </sld:PolygonSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>eti_España</sld:Name>
                    <sld:Title>Texto España</sld:Title>
                    <sld:Abstract>Arial 12 negrita.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                            <ogc:PropertyName>nationallevel</ogc:PropertyName>
                            <ogc:Literal>%1stOrder</ogc:Literal>
                        </ogc:PropertyIsLike>
                    </ogc:Filter>
                    <sld:MinScaleDenominator>1.5E7</sld:MinScaleDenominator>
                    <sld:TextSymbolizer>
                        <sld:Label>
                            <ogc:PropertyName>nameunit</ogc:PropertyName>
                        </sld:Label>
                        <sld:Font>
                            <sld:CssParameter name="font-family">Arial</sld:CssParameter>
                            <sld:CssParameter name="font-size">12</sld:CssParameter>
                            <sld:CssParameter name="font-style">normal</sld:CssParameter>
                            <sld:CssParameter name="font-weight">bold</sld:CssParameter>
                        </sld:Font>
                        <sld:LabelPlacement>
                            <sld:PointPlacement>
                                <sld:AnchorPoint>
                                    <sld:AnchorPointX>0.0</sld:AnchorPointX>
                                    <sld:AnchorPointY>0.0</sld:AnchorPointY>
                                </sld:AnchorPoint>
                                <sld:Displacement>
                                    <sld:DisplacementX>0.0</sld:DisplacementX>
                                    <sld:DisplacementY>0.0</sld:DisplacementY>
                                </sld:Displacement>
                            </sld:PointPlacement>
                        </sld:LabelPlacement>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#262526</sld:CssParameter>
                        </sld:Fill>
                        <sld:VendorOption name="autoWrap">85</sld:VendorOption>
                        <sld:VendorOption name="group">true</sld:VendorOption>
                        <sld:VendorOption name="spaceAround">5</sld:VendorOption>
                    </sld:TextSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>eti_ccaa</sld:Name>
                    <sld:Title>Etiquetas de Comunidades Autónomas</sld:Title>
                    <sld:Abstract>Arial 11 normal.</sld:Abstract>
                    <ogc:Filter>
                        <ogc:And>
                            <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                                <ogc:PropertyName>nationallevel</ogc:PropertyName>
                                <ogc:Literal>%2ndOrder</ogc:Literal>
                            </ogc:PropertyIsLike>
                            <ogc:Not>
                                <ogc:PropertyIsLike wildCard="%" singleChar="_" escape="\">
                                    <ogc:PropertyName>nameunit</ogc:PropertyName>
                                    <ogc:Literal>Conjurisdicciones%</ogc:Literal>
                                </ogc:PropertyIsLike>
                            </ogc:Not>
                        </ogc:And>
                    </ogc:Filter>
                    <sld:MaxScaleDenominator>1.5E7</sld:MaxScaleDenominator>
                    <sld:TextSymbolizer>
                        <sld:Label>
                            <ogc:PropertyName>nameunit</ogc:PropertyName>
                        </sld:Label>
                        <sld:Font>
                            <sld:CssParameter name="font-family">Arial</sld:CssParameter>
                            <sld:CssParameter name="font-size">11</sld:CssParameter>
                            <sld:CssParameter name="font-style">normal</sld:CssParameter>
                            <sld:CssParameter name="font-weight">normal</sld:CssParameter>
                        </sld:Font>
                        <sld:LabelPlacement>
                            <sld:PointPlacement>
                                <sld:AnchorPoint>
                                    <sld:AnchorPointX>0.5</sld:AnchorPointX>
                                    <sld:AnchorPointY>0.5</sld:AnchorPointY>
                                </sld:AnchorPoint>
                                <sld:Displacement>
                                    <sld:DisplacementX>0.0</sld:DisplacementX>
                                    <sld:DisplacementY>0.0</sld:DisplacementY>
                                </sld:Displacement>
                            </sld:PointPlacement>
                        </sld:LabelPlacement>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#000000</sld:CssParameter>
                        </sld:Fill>
                        <sld:VendorOption name="autoWrap">85</sld:VendorOption>
                        <sld:VendorOption name="group">true</sld:VendorOption>
                        <sld:VendorOption name="spaceAround">5</sld:VendorOption>
                    </sld:TextSymbolizer>
                </sld:Rule>
            </sld:FeatureTypeStyle>
        </sld:UserStyle>
        <sld:UserStyle>
            <sld:Name>AU.AdministrativeUnit.Default</sld:Name>
            <sld:Title>Estilo por defecto de Unidad Administrativa</sld:Title>
            <sld:Abstract>La Unidad Administrativa se renderiza utilizando un relleno amarillo(#FFFF66).</sld:Abstract>
            <sld:FeatureTypeStyle>
                <sld:Name>AU.AdministrativeBoundary.Default</sld:Name>
                <sld:Rule>
                    <sld:Name>AU.AdministrativeUnit.Default</sld:Name>
                    <sld:Title>Estilo por defecto de Unidad Administrativa</sld:Title>
                    <sld:Abstract>La Unidad Administrativa se renderiza utilizando un relleno amarillo(#FFFF66).</sld:Abstract>
                    <sld:PolygonSymbolizer>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#FFFF66</sld:CssParameter>
                        </sld:Fill>
                    </sld:PolygonSymbolizer>
                </sld:Rule>
            </sld:FeatureTypeStyle>
        </sld:UserStyle>
    </sld:NamedLayer>
</sld:StyledLayerDescriptor>